- `jackd` must be running

- run `make`

- needs ~2.5GB space

- `make binaural` version may be out of sync and require editing for latency,
  also kills ecasound processes.  FIXME

- `ambix_binaural_standalone_o3` must be running with JACK with a preset loaded
