all: three-algorithms.mkv three-algorithms.pdf

binaural: three-algorithms-binaural.mkv

clean:
	-rm *.mkv *.wav *.pgm *.png *.pdf *.aux *.log mandelbrot

three-algorithms.mkv: mandelbrot-algorithm.mkv
	ffmpeg -i mandelbrot-algorithm.mkv -vf pad=1920:1080:0:60 -pix_fmt yuv420p -profile:v high -level:v 4.1 -crf:v 20 -codec:a copy -max_muxing_queue_size 99999 -y three-algorithms.mkv

three-algorithms.pdf: three-algorithms.md
	pandoc -Vgeometry=margin=2cm three-algorithms.md -o three-algorithms.pdf

three-algorithms-binaural.mkv: mandelbrot-binaural.mkv
	mv mandelbrot-binaural.mkv three-algorithms-binaural.mkv

mandelbrot-binaural.mkv: mandelbrot-binaural.wav three-algorithms.mkv
	ffmpeg -i mandelbrot-binaural.wav -i three-algorithms.mkv -map:a 0:0 -map:v 1:0 -codec:a copy -codec:v copy -shortest mandelbrot-binaural.mkv

mandelbrot-binaural.wav: mandelbrot-algorithm.pcm.wav
	ecasound -f:f32_le,2,48000  -G:jack,,send -i:jack,ambix_binaural_standalone_o3 -o:mandelbrot-binaural.wav &
	ecasound -f:f32_le,16,48000 -G:jack,,recv -i:mandelbrot-algorithm.pcm.wav -o:jack,ambix_binaural_standalone_o3
	killall ecasound

mandelbrot-algorithm.pcm.wav: mandelbrot-algorithm.wav
	sox mandelbrot-algorithm.wav mandelbrot-algorithm.wavpcm
	mv mandelbrot-algorithm.wavpcm mandelbrot-algorithm.pcm.wav

mandelbrot-algorithm.wav: three-algorithms.mkv
	ffmpeg -i three-algorithms.mkv -codec:a copy mandelbrot-algorithm.wav

mandelbrot-algorithm.mkv: titles.1.mkv titles.2.mkv titles.3.mkv mandelbrot-algorithm-0.mkv titles.4.mkv mandelbrot-algorithm-1.mkv titles.5.mkv mandelbrot-algorithm-2.mkv titles.6.mkv
	ffmpeg -i mandelbrot-algorithm.txt -codec:a copy -codec:v copy mandelbrot-algorithm.mkv

mandelbrot: mandelbrot.c
	gcc -std=c99 -Wall -Wextra -pedantic -O3 -fopenmp -o mandelbrot mandelbrot.c -lsndfile -lm

mandelbrot-algorithm-%.mkv: mandelbrot
	-rm out.wav video.mkv
	./mandelbrot $* 128 480 240 0 0 2 | ffmpeg -framerate 60 -f image2pipe -i - -sws_flags neighbor -s 1920x960 -codec:v png video.mkv
	ffmpeg -i out.wav -i video.mkv -codec:v copy -codec:a copy $@

silence.wav:
	ecasound -f:f32_le,16,48000 -i:jack -o:silence.wav -t:4

titles.pdf: titles.tex
	pdflatex titles.tex

titles.1.png titles.2.png titles.3.png titles.4.png titles.5.png titles.6.png: titles.pdf
	gs -sDEVICE=pngalpha -dTextAlphaBits=4 -o titles.%d.png -r960 titles.pdf

%.pgm: %.png
	convert $< -background white $@

%.mkv: %.pgm silence.wav
	-rm t.?.pgm
	ln -s $< t.0.pgm
	ln -s $< t.1.pgm
	ffmpeg -i silence.wav -r 1/4 -i t.%d.pgm -pix_fmt rgb24 -vf negate,fps=fps=60,fade=in:0:60,fade=out:180:60 -r 60 -codec:v png -codec:a copy -t 4 $@
